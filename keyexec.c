//  Copyright 2007-2010 Mir Calculate Ltd. http://www.calculate-linux.org
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// for password
#include <sys/types.h>
#include <pwd.h>
#include <sys/wait.h>

// for strcpy strlen
#include <string.h>

// for keystore kernel
#include <keyutils.h>

// for statistics file
#include <fcntl.h>
#include <sys/stat.h>

int main( int argc, const char* argv[] )
{
  // user id and gid
  uid_t uid, gid;
  int ret;
  int rez1,rez2;
  void *buffer;
  if (argc==1)
    {
      // get user id and gid
      uid = getuid();
      gid = getgid();
      // set user id and gid for the process
      rez1 = setgid(gid);
      rez2 = setuid(uid);
      if (rez1==-1||rez2==-1)
        {
          printf ("exec not SUID root\n");
          exit(EXIT_FAILURE);
        };
      // find user key in keystore
      ret = request_key("user", "tmp", NULL, 0);
      if (ret < 0)
        {
          printf ("id_key not found\n");
          exit(EXIT_FAILURE);
        };
      // get user key
      int retf;
      retf = keyctl_read_alloc(ret, &buffer);
      if (retf < 0)
        {
          printf("error keyctl_read_alloc\n");
          exit(EXIT_FAILURE);
        };
      printf ("%s", (char*) buffer);
      buffer = "XXXXXXXX";
      key_serial_t dest;
      // get id user key
      dest = KEY_SPEC_USER_SESSION_KEYRING;
      // write key in user space
      ret = add_key("user", "tmp", buffer, strlen(buffer), dest);
      exit(EXIT_SUCCESS);
    };
  if (argc!=3)
    {
      printf("Error: needed two argument\n");
      exit(EXIT_FAILURE);
    }

  // get user id and gid
  uid = getuid();
  gid = getgid();

  char * prog_name[11];
  char * prog_path[11];
  char * prog_sring[11];
  int count_prog =11;

  // identifiers and paths to programs
  prog_name[0] = "rdesktop";
  prog_path[0] = "/usr/bin/rdesktop";
  prog_sring[0] = NULL;
  prog_name[1] = "rdesktop1";
  prog_path[1] = "/usr/bin/rdesktop";
  prog_sring[1] = "/usr/bin/kstart --window=.* --desktop=1";
  prog_name[2] = "rdesktop2";
  prog_path[2] = "/usr/bin/rdesktop";
  prog_sring[2] = "/usr/bin/kstart --window=.* --desktop=2";
  prog_name[3] = "rdesktop3";
  prog_path[3] = "/usr/bin/rdesktop";
  prog_sring[3] = "/usr/bin/kstart --window=.* --desktop=3";
  prog_name[4] = "rdesktop4";
  prog_path[4] = "/usr/bin/rdesktop";
  prog_sring[4] = "/usr/bin/kstart --window=.* --desktop=4";
  prog_name[5] = "rdesktop5";
  prog_path[5] = "/usr/bin/rdesktop";
  prog_sring[5] = "/usr/bin/kstart --window=.* --desktop=5";
  prog_name[6] = "rdesktop6";
  prog_path[6] = "/usr/bin/rdesktop";
  prog_sring[6] = "/usr/bin/kstart --window=.* --desktop=6";
  prog_name[7] = "rdesktop7";
  prog_path[7] = "/usr/bin/rdesktop";
  prog_sring[7] = "/usr/bin/kstart --window=.* --desktop=7";
  prog_name[8] = "rdesktop8";
  prog_path[8] = "/usr/bin/rdesktop";
  prog_sring[8] = "/usr/bin/kstart --window=.* --desktop=8";
  prog_name[9] = "rdesktop9";
  prog_path[9] = "/usr/bin/rdesktop";
  prog_sring[9] = "/usr/bin/kstart --window=.* --desktop=9";

  prog_name[10] = "xfreerdp";
  prog_path[10] = "/usr/bin/xfreerdp";
  prog_sring[10] = NULL;
  // path to the running program
  char * str_prog = NULL;
 // if  kstart
  char * str_prog_ks = NULL;
  
  int i;
  for (i=0;i<count_prog;i++)
    {
      if (strcmp(prog_name[i],argv[1])==0)
        {
          str_prog = prog_path[i];
          str_prog_ks = prog_sring[i];
          break;
        };
    };

  if (str_prog == NULL)
    {
      printf ("False program\n");
      exit(EXIT_FAILURE); 
    };

  struct stat bufS;
  int res;
  int fd;
  // file permissions for check
  int mode_file = 33261;
  fd = open(str_prog, O_RDONLY);
  res = fstat(fd,&bufS);

  if (res==0)
    {
      close(fd);
    }else
    {
      printf("No open file %s\n",str_prog);
      exit(EXIT_FAILURE);
    };
  // comparison of permissions and owner of the executable file with the sample
  if (bufS.st_mode == mode_file && bufS.st_uid == 0 && bufS.st_gid == 0)
    {
      struct passwd *pwd = getpwuid (uid);
      if (pwd == NULL)
        {
          exit(EXIT_FAILURE);
        };
      // get user name
      char *login;
      login = (char*) malloc (strlen(pwd->pw_name)+1);
      strcpy (login,pwd->pw_name);

      // set permissions root
      rez1 = setgid(0);
      rez2 = setuid(0);

      if (rez1==-1||rez2==-1)
        {
          printf ("Exec not SUID root\n");
          exit(EXIT_FAILURE);
        };

      int ret;
      // find user key in keystore
      ret = request_key("user", login, NULL, 0);
      if (ret < 0)
        {
          printf ("id_key not found\n");
          exit(EXIT_FAILURE);
        };

      // get user key
      ret = keyctl_read_alloc(ret, &buffer);
      
      if (ret < 0)
        {
          printf("error keyctl_read_alloc\n");
          exit(EXIT_FAILURE);
        }
      // set user id and gid for the process
      rez1 = setgid(gid);
      rez2 = setuid(uid);
      if (rez1==-1||rez2==-1)
        {
          printf ("exec not SUID root\n");
          exit(EXIT_FAILURE);
        };
      key_serial_t dest;  
      // get id user key
      dest = KEY_SPEC_USER_SESSION_KEYRING;
      
      // write key in user space
      ret = add_key("user", "tmp", buffer, strlen(buffer), dest);
      
      // allocate memory and create a line running
      char *buff;
      
      char password[50];
      char *pass = (char *) buffer;
      strcpy(password, pass); // Копируем строку в массив пароля
      strcat(password, "\n"); // Добавляем символ новой стноки
      
      
      // Создаем pipe для передачи пароля в поток
      int pipe_fd[2];
      if (pipe(pipe_fd) == -1) {
          perror("pipe");
          exit(EXIT_FAILURE);
      }

      // Записываем пароль в pipe
      if (write(pipe_fd[1], password, strlen(password)) == -1) {
          perror("write to pipe");
          exit(EXIT_FAILURE);
      }

      // Закрываем записывающий конец pipe
      close(pipe_fd[1]);

      // Создаем дочерний процесс
      pid_t pid = fork();
      if (pid == -1) {
          perror("fork");
          exit(EXIT_FAILURE);
      }
      
      if (str_prog_ks == NULL)
        {
          char *com = "%s %s";
          buff = (char*) malloc (strlen(com)+strlen(str_prog)+strlen(argv[2])+1);
          sprintf (buff, com, str_prog, argv[2]);
          if (pid == 0) {
              // Дочерний процесс
              if (dup2(pipe_fd[0], STDIN_FILENO) == -1) {
                  perror("dup2");
                  exit(EXIT_FAILURE);
              }

              // Выполняем xfreerdp, ожидая пароль на стандартном вводе
              system(buff);

              // Если execlp возвращает управление, значит произошла ошибка
              perror("execlp");
              exit(EXIT_FAILURE);
          } else {
              // Родительский процесс
              close(pipe_fd[0]); // Закрываем читающий конец pipe
              wait(NULL); // Ждем завершения дочернего процесса
          }
        }
      else
        {
          char *com = "%s keyexec | %s %s";
          buff = (char*) malloc (strlen(str_prog_ks)+strlen(com)+strlen(buffer)+strlen(str_prog)+strlen(argv[2])+1);
          sprintf (buff, com, str_prog_ks, str_prog, argv[2]);
          // running program
          system(buff);
        };
      free(login);
      free (buff);
      exit(EXIT_SUCCESS);
    };
  printf ("Executed file %s not valid\n",str_prog);
  exit(EXIT_FAILURE);
}
