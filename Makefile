INSTALL		:= install
DESTDIR		:=


keyexec: keyexec.c
	$(CC) keyexec.c -o keyexec -lkeyutils
	
install: keyexec
	$(INSTALL) -D -m 4711 keyexec $(DESTDIR)/usr/bin/keyexec
	
uninstall: $(DESTDIR)/usr/bin/keyexec
	$(RM) $(DESTDIR)/usr/bin/keyexec
	
clean: keyexec
	$(RM) keyexec

